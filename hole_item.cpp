#include "hole_item.h"
#include "StdAfx.h"


HoleItem::HoleItem(QPoint po, int ang, QColor cl)
    : pos(po)
    , angle(ang)
    , color(cl)
{

}

QRectF HoleItem::boundingRect() const
{
    qreal penWidth = 1;
    qreal x=0, y=0, width=0, height=0;
    x      = (pos.x()-x)- penWidth/2;
    y      = (pos.y()-y) -penWidth/2;
    width  = 2*x + penWidth;
    height = 2*y + penWidth;
    return QRectF(x, y, width, height);
}

void HoleItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->save();                               //保存painter的状态
    painter->translate(pos.x(),pos.y());           //把坐标系统平移到pos为原点
    painter->setRenderHint(QPainter::Antialiasing);//开启抗锯齿
    painter->rotate(angle);                        //把坐标系统旋转angle度
    //QBrush brush(color,Qt::Dense4Pattern);
    //painter->setBrush(brush);                    //使用画刷

    QLinearGradient lg4(QPointF(17,17),QPointF(909,461));//线性渐变
    lg4.setColorAt(0,QColor(0,0,0,250));
    lg4.setColorAt(0.6,QColor(50,0,40,250));
    lg4.setColorAt(1,QColor(0,170,0,250));
    lg4.setSpread(QRadialGradient::PadSpread);
    painter->setBrush(lg4);

    painter->drawEllipse(QPointF(0.0,0.0), x+2, y+1);
    painter->restore();                            //恢复painter的状态
}
