#include "ball_item.h"
#include "StdAfx.h"
#include <QSound>

static const double Pi = 3.14159265358979323846264338327950288419717;
static double TwoPi = 2.0 * Pi;

bool BallItem::m_isWhiteBallShow = true;
QPointF BallItem::m_virScnPos = QPointF(0.0,0.0);

bool IsFirstCollide = true;
bool FoulFlag = false;
bool HaveEntered = false;
bool MissingBall = false;

static qreal normalizeAngle(qreal angle)
{
    //把angle转为0-2PI
    while (angle < 0)
        angle += TwoPi;
    while (angle > TwoPi)
        angle -= TwoPi;
    return angle;
}

float distance(Vector2 p1, Vector2 p2)
{

    return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y) );
}
float distance(QPointF p1, QPointF p2)
{
	return sqrt((p2.x() - p1.x()) * (p2.x() - p1.x()) + (p2.y() - p1.y()) * (p2.y() - p1.y()) );

}

void BallItem::cdBallBoundary(float w/* = 893*/, float h/* = 445*/)
{
	float boundaryWidth  = 20;
    if( pos.x() <= radius + boundaryWidth  )
        dir.x = abs(dir.x);
    if( pos.x() >= w - radius + boundaryWidth - 5)
        dir.x = -abs(dir.x);
    if( pos.y() <= radius + boundaryWidth )
        dir.y = abs(dir.y);
    if(pos.y() >= h  - radius + boundaryWidth - 5)
        dir.y = -abs(dir.y);
}

void BallItem::cdBallHole()
{    
	for(int i = 0; i < 6; ++i)
	{		
        if( distance( pos, pBallSystem->hole[i]->getPos()) < 15  && visionFlag) { //已进的球不再检测
			visionFlag = false;
            hide();
            pBallSystem->holeEnteredBall.push_back(this);
        }
    }
}

int MyMin(const int m, const int first, const int should)
{
    if (first != should) {
        return should >= abs(m) ? (0-should) : m;
    }
    return 0;
}

void BallItem::cdBallBall()
{
    if(!pBallSystem)
        return;

    if(collideOrders.empty()) {
		return ;
    }

    for (int i = 0; i < collideOrders.size() ; ++i)
    {   
		BallItem *iter = pBallSystem->ball[collideOrders[i] - 1];
        if (!visionFlag) {
            return;
        }
        if(*iter != *this)
        {

            QLineF distance_line(pos, iter->pos);

            Vector2 tempPos1, tempPos2;
            tempPos1.x = pos.x();
            tempPos1.y = pos.y();
            tempPos2.x = iter->pos.x();
            tempPos2.y = iter->pos.y();

            Vector2 srcToDes = tempPos2 - tempPos1;
            srcToDes = srcToDes.normalize();
            dir = dir.normalize();
            float srccos = dir * srcToDes;
            if(abs(srccos) > 1)
                srccos = srccos > 0 ? 1.0f : -1.0f;
            float srcsin = sqrt(1 - srccos * srccos);

            Vector2 srcPer = dir - srcToDes * srccos;
            srcPer = srcPer.normalize();//垂直方向的方向向量

            float srcVelParCom = vel * srccos;
            float srcVelPerCom = vel * srcsin;

            iter->dir = iter->dir.normalize();
            float descos = iter->dir * srcToDes;
            if(abs(descos) > 1)
                descos = descos > 0 ? 1.0f : -1.0f;
            float dessin = sqrt(1 - descos * descos);

            Vector2 desPer = iter->dir - srcToDes * descos;
            desPer = desPer.normalize();

            float desVelParCom = iter->vel * descos;
            float desVelPerCom = iter->vel * dessin;

            //碰撞后平行方向速度交换
            float relativeVel = srcVelParCom - desVelParCom;
            if( relativeVel > 0)//证明他两还在靠拢
            {
                float temp = srcVelParCom;
                srcVelParCom = desVelParCom;
                desVelParCom = temp;
            }

            //再算速度的方向和大小

            Vector2 srcFinal = srcToDes * srcVelParCom + srcPer * srcVelPerCom;
            Vector2 desFinal = srcToDes * desVelParCom + desPer * desVelPerCom;

            dir = srcFinal.normal();
            vel = sqrt(srcFinal.x * srcFinal.x + srcFinal.y * srcFinal.y);
            iter->dir = desFinal.normal();
            iter->vel = sqrt(desFinal.x * desFinal.x + desFinal.y * desFinal.y);

            if (m_whichBall==whiBall && IsFirstCollide) { //第一次和白球碰撞的
                IsFirstCollide = false; //避免白球多次碰撞后重复检测
                MissingBall = false; //不是空杆
                //把一次与白球碰撞球的颜色记录下来
                pBallSystem->firstCollidedBallColor = iter->color; //第一次与白球碰撞球的颜色                
            }            
        }
    }    
}

BallItem::BallItem(SnookerScene *p, QColor col, QPointF po, unsigned int i, float v/* = 0.0f*/, Vector2 d/* = Vector2(0.0f, 0.0f)*/,
                   float m/* = 2.0f*/, float r/* = 20.0f*/, float w/* = 640*/, float h/* = 480*/)
{
    pBallSystem = p;
    order = i;
    originPos = pos = po;
    vel = v;
	visionFlag = true;
    if(d.x * d.x + d.y * d.y < EPISION)
        dir = Vector2(0.0f, 0.0f);
    else
        dir = d / sqrt(d.x * d.x +d.y * d.y);
    mass = m;
    radius = r;
    width = w;
    height = h;

    m_virtualFlag = false;
    color = col;
    if (col == Qt::white) {
        m_whichBall = whiBall;
    } else if(col == Qt::yellow) {
        m_whichBall = yelBall;
    } else if(col == Qt::green) {
        m_whichBall = greBall;
    } else if(col == Qt::blue) {
        m_whichBall = bluBall;
    } else if(col == Qt::darkYellow) {
        m_whichBall = broBall;
    } else if(col == Qt::magenta) {
        m_whichBall = pinBall;
    } else if(col == Qt::black) {
        m_whichBall = blaBall;
    } else {
        m_whichBall = redBall;
    }
    if (!m_virtualFlag) {
        setPos(getPosValue());
    }
}

QRectF BallItem::boundingRect() const
{
    qreal penWidth = 0.1;
    return QRectF((0-radius)-penWidth/2,
                  (0-radius)-penWidth/2,
                  2*radius + penWidth,
                  2*radius + penWidth
                  );
}

QPainterPath BallItem::shape() const
{
    QPainterPath path;
    path.addRect(0-radius, 0-radius, 2*radius, 2*radius);
//    path.addEllipse(0,0,radius,radius);
    return path;
}

void BallItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    if(m_virtualFlag)
    {
        QPen pen(Qt::black,0.5,Qt::DashDotLine,Qt::RoundCap,Qt::RoundJoin);
        painter->setPen(pen);
    } else {
        painter->setRenderHint(QPainter::Antialiasing);//开启抗锯齿
        QBrush brush(color,Qt::Dense4Pattern);
        painter->setBrush(brush);                    //使用画刷
        QRadialGradient radialgradient(posA,radius,QPointF(posA.x(),posA.y()+radius));//辐射渐变
        radialgradient.setColorAt(0,color);
        radialgradient.setColorAt(1,color);
        radialgradient.setSpread(QRadialGradient::RepeatSpread);//渐变区域以外的扩散方式
        painter->setBrush( radialgradient);
    }
    painter->drawEllipse(posA,radius,radius);
}


void BallItem::setColorValue(QColor &col) //给color赋值
{
    color=col;
}

void BallItem::setPosValue(const QPointF &po)   //给pos赋值
{
    pos=po;
    setPos(pos);
}

void BallItem::setVirtualFlag(bool x)
{
    m_virtualFlag = x;   
    if(m_virtualFlag)
    {       
        m_whichBall = virBall;
        setVisible(false);        
    }
}

void BallItem::setIsWhiteBallShow(bool x)
{
    m_isWhiteBallShow = x;
}

QColor BallItem::getColorValue()
{
    return color;
}

QPointF BallItem::getPosValue()
{
    return pos;
}

void BallItem::virtuallBallMove(QPointF mousePos)//鼠标移动槽函数
{
    //tableItem把鼠标位置发给虚球与白球,在这里接收处理
    if(m_whichBall == virBall)//是虚球
    {
        if(m_isWhiteBallShow)//白球为显示状态
        {
            setVisible(true);//使虚球显示,跟着鼠标移动
            QPointF mouseToVirPos = mapFromScene(mousePos);
            moveBy(mouseToVirPos.x(), mouseToVirPos.y());
            pos = m_virScnPos = mousePos;
        }
    }
}

void BallItem::whiteBallMove(QPointF mousePos)
{
    if(m_whichBall == whiBall)//是白球
    {
        if(!m_isWhiteBallShow)//白球为不为显示状态
        {
            setVisible(true);//使白球显示跟着鼠标移动
            pBallSystem->ball[6]->pos = mousePos;
            setPos(mousePos);
        }
    }
}

void BallItem::mouseIsPressed(bool x)
{
    if(x && (m_whichBall==virBall))//鼠标按下且是虚球
    {
        setVisible(false);
    }
    if(x && (m_whichBall == whiBall))
    {
        if (IsSwingBall) {
            //检测在开球区
            float tempradius = pBallSystem->ball[0]->originPos.y() - pBallSystem->ball[1]->originPos.y();
            if (distance(pos, pBallSystem->ball[1]->originPos) < tempradius
                    && pos.x()<pBallSystem->ball[1]->originPos.x())
            {
                m_isWhiteBallShow = true;
                IsSwingBall = false;
            }
        }

    }

}


void BallItem::MoveBall(float fElapsedTime)
{
    if( vel < EPISION ) {
        return;
    }

    vel -= (0.5 * 2000 * fElapsedTime); //摩擦力
    if( vel < EPISION ) {
        return;
    }

    dir.normalize();

    float nextX = pos.x() + dir.x * fElapsedTime * vel * 500;
    float nextY = pos.y() + dir.y * fElapsedTime * vel * 500;

    collideOrders.clear();
    for (int i = 0; i < BALL_NUMBER; ++i)
    {
        BallItem *iter = pBallSystem->ball[i];

        if(*iter != *this)
        {
            Vector2 tempPos1, tempPos2;
            tempPos1.x = nextX;
            tempPos1.y = nextY;
            tempPos2.x = iter->pos.x();
            tempPos2.y = iter->pos.y();
            if(distance(tempPos1, tempPos2) <= radius + iter->radius + EPISION )
                collideOrders.push_back( iter->order );//将碰撞球的序号加向量中
            int i = 0;
            while(distance(tempPos1, tempPos2) <= radius + iter->radius + EPISION)//两球相撞
            {

                Vector2 desToSrc = tempPos1 - tempPos2;
                desToSrc = desToSrc.normalize();
                tempPos1 = tempPos2 + desToSrc * ( radius + iter->radius + EPISION + i * 0.1);
                ++i;
            }
            nextX = tempPos1.x;
            nextY = tempPos1.y;
        }
    }

    setPosValue(QPointF(nextX, nextY));

	cdBallBall(); //检测碰撞
    cdBallBoundary();//边界检测
	cdBallHole();//球洞检测    
}

void BallItem::advance(int step)
{
    if (!step) {
        return;
    }

    MoveBall(0.0003);
}




