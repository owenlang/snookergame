#ifndef START_BUTTON_H
#define START_BUTTON_H

#include <QPushButton>

class StartButton : public QPushButton
{
    Q_OBJECT
public:
    explicit StartButton(QWidget *parent = 0);
    virtual ~StartButton();
    void initStyle();
    bool getIsPause();
signals:
    void pause();
    void resume();
public slots:
    void clicked();

private:
    bool isPause;
};

#endif // START_BUTTON_H
