#ifndef CONSTANT_H
#define CONSTANT_H

#include<QPointF>
#include<QPixmap>

#define EPISION 0.00001
#define BALL_NUMBER  22
#define ELAPSED_TIME  40

const int ballRadius = 8;       //球半径
const int totalHeight = 478;     //桌子总高
const int totalWidth = 926;    //桌子总宽
const int LOG_WIDTH = 300;

const int PRINT_LOG = 999;

extern bool IsSwingBall; //是否重新放置白球
extern bool IsFirstCollide;//第一次碰撞检测
extern bool FoulFlag; //犯规标志
extern bool HaveEntered; //是否有球进洞
extern bool MissingBall; //空杆



/******************POINTS***********************/
const QPointF posA        = QPointF(0.0,0.0);      //
const QPointF posB        = QPointF(926.0,0.0);    //
const QPointF posC        = QPointF(926.0,478.0);  //
const QPointF posD        = QPointF(0.0,478.0);    //

const QPointF posA1       = QPointF(17.0,17.0);    //
const QPointF posB1       = QPointF(909.0,17.0);   //
const QPointF posC1       = QPointF(909.0,461.0);  //
const QPointF posD1       = QPointF(17.0,461.0);   //

const QPointF posE        =QPointF(201.0,17.0);    //开球线
const QPointF posE1       =QPointF(201.0,461.0);   //开球线
const QPointF posGreen    = QPointF(201.0,154.0);  //
const QPointF posBrown    = QPointF(201.0,227.0);  //
const QPointF posYellow   = QPointF(201.0,300.0);  //
const QPointF posBlue     = QPointF(463.0,227.0);  //
const QPointF posPink     = QPointF(645.0,227.0);  //
const QPointF posBlack    = QPointF(828.0,227.0);  //

const QPointF posRed[15]={          //X+14 Y+/-8
    QPointF(645.0+2+16,227.0),//红0
    QPointF(677.0,219.0),     //红1
    QPointF(677.0,235.0),     //红2
    QPointF(691.0,211.0),     //红3
    QPointF(691.0,227.0),     //红4
    QPointF(691.0,243.0),     //红5
    QPointF(705.0,203.0),     //红6
    QPointF(705.0,219.0),     //红7
    QPointF(705.0,235.0),     //红8
    QPointF(705.0,251.0),     //红9
    QPointF(719.0,195.0),     //红10
    QPointF(719.0,211.0),     //红11
    QPointF(719.0,227.0),     //红12
    QPointF(719.0,243.0),     //红13
    QPointF(719.0,259.0),     //红14
};

/******************POINTS***********************/


/******************MESSAGE**********************/
/******************MESSAGE**********************/


#endif // CONSTANT_H
