//球类
#ifndef BALLITEM_H
#define BALLITEM_H

#include <QGraphicsObject>
#include "constant.h"
#include "vector2.h"
#include <vector>
using std::vector;
class SnookerScene;
class BallItem : public QGraphicsObject
{
    Q_OBJECT
public:
    enum BallColor{virBall,whiBall,yelBall,broBall,
              greBall,bluBall,pinBall,blaBall,redBall};

    bool         m_virtualFlag; //生成的对象是否是虚球
    enum BallColor m_whichBall;   //判断生成的对象是什么颜色的球
    static bool      m_isWhiteBallShow;     //白球是显示状态
    static QPointF   m_virScnPos;           //虚球的位置(也是鼠标的位置）

    QColor color;         //球的颜色
    unsigned int order;//序数
    QPointF pos;//球心位置
    float vel;//速度
    Vector2 dir;//速度方向
    float mass;//质量
    float radius;//球的半径
    float width;//球台宽度
    float height;//球台高度
	bool visionFlag;//是否已进洞
    SnookerScene *pBallSystem;
	vector<int> collideOrders;//记录碰撞球的序号
    QPointF originPos;

public:    
    explicit BallItem(SnookerScene *p, QColor col, QPointF po, unsigned int i, float v = 0.0f, Vector2 d = Vector2(0.0f, 0.0f),
                      float m = 2.0f, float r = 8.0f, float w = 893, float h = 445);    

    QPainterPath shape() const;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void setColorValue(QColor &col);
    void setPosValue(const QPointF &po);
    void setVirtualFlag(bool x);
    void setIsWhiteBallShow(bool x);
    QColor getColorValue();
    QPointF getPosValue();

    bool operator = ( const BallItem& b) const
    {
        return order == b.order;
    }
    bool operator != ( const BallItem& b) const
    {
        if (order==0 || b.order==0) { //0号是虚球
            return false;
        }
        return order != b.order;
    }

    void MoveBall(float fElapsedTime);
    void cdBallBall();
	void cdBallHole();
    void cdBallBoundary(float w = 893, float h = 445);

protected:
    void advance(int step);
signals:
    void mousePos(QPointF);
private slots:
    void virtuallBallMove(QPointF mousePos);
    void whiteBallMove(QPointF mousePos);
    void mouseIsPressed(bool x);
};

#endif // BALLITEM_H
