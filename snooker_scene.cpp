#include "snooker_scene.h"
#include "StdAfx.h"

bool IsSwingBall = true;

SnookerScene::SnookerScene(QObject *parent) :
    QGraphicsScene(parent)
{    
    initData();
}

void SnookerScene::moveCue(QPointF pos)
{
    _cue->setLine(pos.x(), pos.y(), ball[6]->x(), ball[6]->y());
}

void SnookerScene::hideCue()
{
    _cue->hide();
    _timeRemainSlider->resetValue();
}
void SnookerScene::showCue()
{
    _cue->show();
}

//返回最小罚分值
int FoulMin(const int m, const int first, const int should)
{
    if (first > m && should > m) {
        return m;
    }
    return first < should ? first : should;
}

//进球是否范规,//注意与彩球的等于判断,只要是其中一个就相等
bool SnookerScene::isFoul(const QColor& entered, const QColor& should)
{
    //进了白球
    if (entered == Qt::white) {
        return true;
    }

    //进了红球
    if (entered==Qt::red) {
        if (entered != should) {
            return true;
        }
    }
    //进了彩球
    if (entered!=Qt::red) {
        enteredWhiteCount ++;
        if (enteredWhiteCount > 1) { //进了多个彩球
            return true;
        }
        if (haveRedBall() && should!=Qt::gray) { //桌上有红球且不应该进彩球
            return true;
        }
    }

    return false;
}

void SnookerScene::advance()
{
    if (isAllBallStop()) {   //等球停止后开始处理
        calcScore();
    }
    QGraphicsScene::advance();
}

Qt::GlobalColor SnookerScene::minScoreBall()
{
    BallItem* yellow_ball = ball[0];//黄球
    BallItem* green_ball  = ball[3];//咖啡球
    BallItem* coffee_ball = ball[1];//蓝球
    BallItem* blue_ball   = ball[2];//绿球
    BallItem* pink_ball   = ball[4];//粉球
    BallItem* black_ball  = ball[5];//黑球
    if (yellow_ball->visionFlag) {        return Qt::yellow;    }
    if (green_ball->visionFlag) {        return Qt::green;    }
    if (coffee_ball->visionFlag) {        return Qt::darkYellow;    }
    if (blue_ball->visionFlag) {        return Qt::blue;    }
    if (pink_ball->visionFlag) {        return Qt::magenta;    }
    if (black_ball->visionFlag) {        return Qt::black;    }
}

void SnookerScene::calcScore()
{
    static int c = 0;
    if (isHitBall) { //isHitBall确保球停止后只进入此处一次
        isHitBall = false;

        if (MissingBall && firstCollidedBallColor==Qt::white && lastBallColor==Qt::white) { //是空杆
            qDebug() << "test";
            FoulFlag = true;
            score = -4;
        }
//////罚分检测
        if (haveRedBall()) { //桌上有红球时
//            if (firstCollidedBallColor!=ballShouldBeHitted && ballShouldBeHitted!=Qt::gray && lastBallColor!=Qt::white) {
//                FoulFlag = true;
//                score = FoulMin(-4, -scoreOfColor(firstCollidedBallColor), -scoreOfColor(ballShouldBeHitted));
//            }
            if (firstCollidedBallColor!=Qt::white && firstCollidedBallColor!=ballShouldBeHitted && ballShouldBeHitted!=Qt::gray) {
                FoulFlag = true;
                score = FoulMin(-4, -scoreOfColor(firstCollidedBallColor), -scoreOfColor(ballShouldBeHitted));
            }
            for (int i = 0, count=0; i < holeEnteredBall.size(); ++i) { //有球进了
                QColor tempcolor = holeEnteredBall[i]->color;
                //只要进的球色有一个不对应，就罚分
                if (tempcolor!=ballShouldBeHitted && ballShouldBeHitted!=Qt::gray) { //不该进彩球时
                    FoulFlag = true;
                    score = FoulMin(-4, -scoreOfColor(tempcolor),   -scoreOfColor(ballShouldBeHitted));
                    break;
                }
                if (tempcolor != Qt::red) { //进了多个彩球，犯规
                    ++count;
                    if (count > 1) {
                        FoulFlag = true;
                        score = FoulMin(-4, -scoreOfColor(tempcolor),   -scoreOfColor(ballShouldBeHitted));
                        break;
                    }
                }

                //加分
                score += scoreOfColor(tempcolor);
            }

            //遍历，把彩球拿出来，并记录最后进球的花色
            for (int j = 0; j < holeEnteredBall.size(); ++j) {
                BallItem *tempptr = holeEnteredBall[j];
                //把彩球拿出来的情况
                if (tempptr->color != Qt::red) { //只要不是红球就拿出来
                        tempptr->visionFlag = true;
                        tempptr->setPosValue(tempptr->originPos);
                        tempptr->vel = 0;
                        tempptr->show();
                        if (tempptr->color == Qt::white) {
                            whiteBall->setIsWhiteBallShow(false);
                            tempptr->setPosValue(tableItem->getMousePos());
                            IsSwingBall = true;
                        }
                }
                //记录最后进的球色
                lastBallColor = tempptr->color;
            }
            if (lastBallColor == Qt::white || lastBallColor != Qt::red) { //上次没进球或上次进的是彩球
                ballShouldBeHitted = Qt::red;
            } else {
                ballShouldBeHitted = Qt::gray;
            }
        } else { //桌上没红球了
//            if (firstCollidedBallColor != ballShouldBeHitted && ballShouldBeHitted!=Qt::gray && lastBallColor!=Qt::white) {
//                FoulFlag = true;
//                score = FoulMin(-4, -scoreOfColor(firstCollidedBallColor), -scoreOfColor(ballShouldBeHitted));
//            }
            if (firstCollidedBallColor!=Qt::white && firstCollidedBallColor!=ballShouldBeHitted && ballShouldBeHitted!=Qt::gray) {
                FoulFlag = true;
                score = FoulMin(-4, -scoreOfColor(firstCollidedBallColor), -scoreOfColor(ballShouldBeHitted));
            }
            for (int i = 0, count=0; i < holeEnteredBall.size(); ++i) { //有球进了
                QColor tempcolor = holeEnteredBall[i]->color;
                //只要进的球色有一个不对应，就罚分
                if (tempcolor!=ballShouldBeHitted && ballShouldBeHitted!=Qt::gray) { //不该进彩球时
                    FoulFlag = true;
                    score = FoulMin(-4, -scoreOfColor(tempcolor),   -scoreOfColor(ballShouldBeHitted));
                    break;
                }
                if (tempcolor != Qt::red) { //进了多个彩球，犯规
                    ++count;
                    if (count > 1) {
                        FoulFlag = true;
                        score = FoulMin(-4, -scoreOfColor(tempcolor),   -scoreOfColor(ballShouldBeHitted));
                        break;
                    }
                }

                //加分
                score += scoreOfColor(tempcolor);
            }

            //遍历，把彩球拿出来，并记录最后进球的花色
            for (int j = 0; j < holeEnteredBall.size(); ++j) {
                BallItem *tempptr = holeEnteredBall[j];
                //把彩球拿出来的情况
                if ((tempptr->color!=Qt::red&&lastBallColor==Qt::red) || //进的不是红球且上一次进的是红球就拿出来
                        (tempptr->color != minScoreBall()) || //或者进的球不是桌上最小分值的
                        (tempptr->color == Qt::white))   //或者进了白球
                {
                        tempptr->visionFlag = true;
                        tempptr->setPosValue(tempptr->originPos);
                        tempptr->vel = 0;
                        tempptr->show();
                        if (tempptr->color == Qt::white) {
                            whiteBall->setIsWhiteBallShow(false);
                            tempptr->setPosValue(tableItem->getMousePos());
                            IsSwingBall = true;
                        }
                }
                //记录最后进的球色
                lastBallColor = tempptr->color;
            }

            if (lastBallColor==Qt::red) { //上次进了红球
                ballShouldBeHitted = Qt::gray;
            } else if (ball[0]->visionFlag) { //黄球还在
                ballShouldBeHitted = Qt::yellow;
            } else  if (ball[3]->visionFlag) { //绿球还在
                ballShouldBeHitted = Qt::green;
            } else if (ball[1]->visionFlag) { //咖啡球
                ballShouldBeHitted = Qt::darkYellow;
            } else if (ball[2]->visionFlag) { //蓝球还在
                ballShouldBeHitted = Qt::blue;
            } else if (ball[4]->visionFlag) { //粉球还在
                ballShouldBeHitted = Qt::magenta;
            } else if (ball[5]->visionFlag) { //黑球还在
                ballShouldBeHitted = Qt::black;
            }
        }

        if (0 == holeEnteredBall.size()) { //没有进球
            lastBallColor = Qt::white;
        }
//////
        //有犯规了，增加犯规次数，打印日志
        if (FoulFlag) {
            ++foulCount;
            goalsBall(QString("Foul, points %1.").arg(score), PRINT_LOG);
        }
        //有进球加分，增加总分，打印日志
        if (score > 0) { //只罚分不扣分
            actuallyScore += score;
            goalsBall(QString("Entered ball, add %1.").arg(score), PRINT_LOG);
        }

        score = 0;//把得分置0
        FoulFlag = false;//把犯规置为假
        MissingBall = true; //开始就是空杆，只要有球碰到白球，就置为假
        holeEnteredBall.clear(); //清空
        showCue(); //显示虚线
        firstCollidedBallColor = Qt::white; //设为无效
        _timeRemainSlider->restartTimer(); //重新开始计时
        enteredWhiteCount = 0;
    }

    //显示右下方日志
    QString curr_score = "Curr Score: " + QString("%1").arg(actuallyScore);
    QString foul_count = "Foul Count:    " + QString("%1").arg(foulCount);
    QString usage_time = "Usage Time:    " + QString("%1").arg(usageTime);
    QString double_hit = "Double Hit:    " + QString("%1").arg(doubleHit);

    recordShow->setPlainText(curr_score);
    recordShow->append(foul_count);
    recordShow->append(usage_time);
    recordShow->append(double_hit);
}

bool SnookerScene::isAllBallStop()
{
    bool res = true;
    for (int i = 0; i < BALL_NUMBER; ++i) {
        if(ball[i]->vel > EPISION && ball[i]->visionFlag) {
            res = false;
            break;
        }
    }
    return res;
}

bool SnookerScene::haveRedBall() //台面上是否有红球
{
    for (int i = 7; i < BALL_NUMBER; ++i) {
        if (ball[i]->visionFlag) {
            return true;
        }
    }
    return false;
}

void SnookerScene::hitBall(int &strength)
{
    for (int i = 0; i < BALL_NUMBER; ++i) {
        if(!isAllBallStop()) { //球还没停止
//            goalsBall("Please wait the ball stop...", PRINT_LOG);
            return;
        }
    }

    whiteBall->vel = strength;
    QPointF virPos = virtualBall->pos;

    whiteBall->dir.x = (virPos-whiteBall->pos).x();
    whiteBall->dir.y = (virPos-whiteBall->pos).y();

    IsFirstCollide = true;

    _timeRemainSlider->stopTimer();
    isHitBall = true;
}


void SnookerScene::initData()
{    
    initTable();
    initHole();
    initBall();
    initSide();
    initSlider();

    _cue = new QGraphicsLineItem();
    _cue->setPen(QPen(Qt::DashDotLine));
    _cue->setLine(0, 0, 0, 0);
    QObject::connect(tableItem, SIGNAL(mouseMove(QPointF)), this, SLOT(moveCue(QPointF)));
    addItem(_cue);

    _timer = new QTimer();
    QObject::connect(_timer, SIGNAL(timeout()), this, SLOT(advance()));
    QObject::connect(_startButton, SIGNAL(pause()), _timer, SLOT(stop()));
    QObject::connect(_startButton, SIGNAL(resume()), _timer, SLOT(start()));
    _timer->start(40);

    immShow = new QTextBrowser();
    recordShow = new QTextBrowser();
    addWidget(immShow)->setPos(totalWidth+1, 1);
    immShow->setFixedSize(LOG_WIDTH, totalHeight/2);

    addWidget(recordShow)->setPos(totalWidth+1, totalHeight/2+1);
    recordShow->setFixedSize(LOG_WIDTH, totalHeight/2-2);

    immShow->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    recordShow->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    lastBallColor = Qt::white;
    ballShouldBeHitted = Qt::red;
    firstCollidedBallColor = Qt::white;

    enteredWhiteCount = 0;
    score = 0;
    actuallyScore = 0;
    foulCount = usageTime = doubleHit = 0;
    usageTimer = new QTimer();
    QObject::connect(usageTimer, SIGNAL(timeout()), this, SLOT(usageTimeSlot()));
    usageTimer->start(1000);

    isHitBall = false;
}
void SnookerScene::usageTimeSlot()
{
    ++usageTime;
}

void SnookerScene::initTable()
{
    tableItem = new TableItem();   //自定义球桌图形项
    this->addItem(tableItem);
}

void SnookerScene::initHole()
{
    //6个球洞
    for(int i = 0; i < 6; ++i) {
        int px, py;//球洞中心的x、y
        int angle = (-45 + 45*(i%3));
        if (0==i||5==i) {
            px = 17+2;//左边两洞的x
        } else if (1==i||4==i) {
             px = 463;//中洞x
        } else {
             px = 909-3;
        }

        if(i<3) {
             py = 17+2;
             if (1==i) {
                 py-=7;
             }
        } else {
             py = 461-2;
             if (4==i) {
                 py+=7;
             }
        }
        //球洞类，参数：中心、旋转角度、颜色、x、y
        hole[i] = new HoleItem(QPoint(px, py), angle, QColor(0,0,255,250));
        this->addItem(hole[i]);
    }
}
void SnookerScene::initSide()
{
    //4个边缘
    QPointF local[8]={//8个点，外圈、内圈、顺时针
        QPointF(0.0,0.0),
        QPointF(926.0,0.0),
        QPointF(926.0,478.0),
        QPointF(0.0,478.0),

        QPointF(17.0,17.0),
        QPointF(909.0,17.0),
        QPointF(909.0,461.0),
        QPointF(17.0,461)
    };
    QPointF points[4][4]={//每个边缘的4的点，共16个
            local[0],local[4],local[7],local[3],//左边缘
            local[1],local[5],local[4],local[0],//上
            local[2],local[6],local[5],local[1],//右
            local[3],local[7],local[6],local[2]//下
            };
    side0 = new SideItem(points[0]);//left
    side1 = new SideItem(points[1]);//up
    side2 = new SideItem(points[2]);//right
    side3 = new SideItem(points[3]);//down
    this->addItem(side0);
    this->addItem(side1);
    this->addItem(side2);
    this->addItem(side3);
}
void SnookerScene::initBall()
{
    ball[0] = new BallItem(this, Qt::yellow, posYellow, 1);//黄球
    ball[1] = new BallItem(this, Qt::darkYellow, posBrown, 2);//咖啡球
    ball[2] = new BallItem(this, Qt::blue, posBlue, 3); //蓝球
    ball[3] = new BallItem(this, Qt::green, posGreen, 4);//绿球
    ball[4] = new BallItem(this, Qt::magenta, posPink, 5);//粉球
    ball[5] = new BallItem(this, Qt::black, posBlack, 6);//黑球
    whiteBall = ball[6] = new BallItem(this, Qt::white,QPointF(posBrown.x()-20, posBrown.y()+20), 7); //白球
    ball[6]->setIsWhiteBallShow(false);

    virtualBall = new BallItem(this, Qt::white, QPointF(0.0,0.0), 0);
    virtualBall->setVirtualFlag(true);
    addItem(virtualBall);


    QObject::connect(tableItem, SIGNAL(mouseMove(QPointF)), virtualBall, SLOT(virtuallBallMove(QPointF)));
    QObject::connect(tableItem, SIGNAL(mouseMove(QPointF)), whiteBall, SLOT(whiteBallMove(QPointF)));
    QObject::connect(tableItem, SIGNAL(mouseIsPressed(bool)), virtualBall ,SLOT(mouseIsPressed(bool)));
    QObject::connect(tableItem, SIGNAL(mouseIsPressed(bool)), whiteBall, SLOT(mouseIsPressed(bool)));
    for(int i = 7, j=0; i < BALL_NUMBER; ++i, ++j) {
        ball[i] = new BallItem(this, Qt::red, posRed[j], 8+j);
    }

    for (int i = 0; i < BALL_NUMBER; ++i) {
        addItem(ball[i]);        
    }
}

void SnookerScene::resetBallPos()
{
    _timer->stop();
    for (int i = 0; i < BALL_NUMBER; ++i) {
        ball[i]->vel = 0;
        ball[i]->visionFlag = true;
        ball[i]->setPosValue(ball[i]->originPos);
        ball[i]->show();
    }

    whiteBall->setIsWhiteBallShow(false);
    IsSwingBall = true;

    _timeRemainSlider->resetValue();
    this->usageTime = 0;
    this->actuallyScore = 0;
    this->foulCount = 0;
    this->doubleHit = 0;    
    _timer->start(40);
}

void SnookerScene::initSlider()
{
    QLabel *label1 = new QLabel(tr("Strength:"));
    addWidget(label1)->setPos(1, totalHeight+7);

    _intensitySlider = new IntensitySlider();
    QGraphicsWidget *strengthSlider = this->addWidget(_intensitySlider);
    strengthSlider->setPos(label1->size().width()+2, totalHeight+5);

    _startButton = new StartButton();
    QGraphicsWidget *startBtn = this->addWidget(_startButton);
    startBtn->setPos(label1->size().width() + strengthSlider->size().width()+4,
                     totalHeight);

    _restartButton = new RestartButton();
    QGraphicsWidget *restartBtn = this->addWidget(_restartButton);
    restartBtn->setPos(label1->size().width() + strengthSlider->size().width() + startBtn->size().width() + 4,
                       totalHeight);

    QLabel* label2 = new QLabel(tr("Remain Time:"));
    addWidget(label2)->setPos(label1->size().width() + strengthSlider->size().width() + startBtn->size().width() + restartBtn->size().width() + 4,
                totalHeight+7);

    _timeRemainSlider = new TimeRemainSlider();
    QGraphicsWidget *timeSlider = this->addWidget(_timeRemainSlider);
    timeSlider->setPos(label1->size().width() + strengthSlider->size().width() + startBtn->size().width() + restartBtn->size().width() + label2->size().width()+ 4,
                       totalHeight+5);

    showLogButton = new ShowLog();
    addWidget(showLogButton)->setPos(label1->size().width() + strengthSlider->size().width() + startBtn->size().width() + restartBtn->size().width()
                                     + label2->size().width()+ timeSlider->size().width() + 4,
                totalHeight);
    QObject::connect(showLogButton, SIGNAL(clicked()), this, SLOT(showLogSlot()));

    QObject::connect(tableItem, SIGNAL(mouseIsPressed(bool)), _intensitySlider, SLOT(mouseIsPressed(bool)));
    QObject::connect(_intensitySlider, SIGNAL(sendStrength(int&)), this, SLOT(hitBall(int&)));
    QObject::connect(_intensitySlider, SIGNAL(sendStrength(int&)), this, SLOT(hideCue()));
    QObject::connect(_restartButton, SIGNAL(clicked()), this, SLOT(resetBallPos()));
    QObject::connect(_timeRemainSlider, SIGNAL(hitBallTimeOut()), this, SLOT(hitBallTimeOut()));
}

void SnookerScene::showLogSlot()
{
    static bool f = false;
    if (f) {
        showLogButton->setText(tr("Show Log"));
        emit hideLog();
    } else {
        showLogButton->setText(tr("Hide Log"));
        emit showLog();
    }
    f = !f;
}

void SnookerScene::hitBallTimeOut()
{    
    goalsBall("Hit ball time out, points " + QString("%1").arg(-4), PRINT_LOG);
    this->foulCount ++;
}

void SnookerScene::goalsBall(const QString &str, const int score)
{
    QString datestr = QDateTime::currentDateTime().toString("[HH:mm:ss] ");

    if (score == PRINT_LOG) {
        immShow->append(datestr + str);
        return;
    }
    QString log = str + ", add score: " + QString("%1").arg(score);
    immShow->append(datestr + log);
}

SnookerScene::~SnookerScene()
{
    delete tableItem;
    for (int i = 0; i < 6; ++i) {
        delete hole[i];
    }
    for (int i = 0; i < BALL_NUMBER; ++i) {
        delete ball[i];
    }
    delete _intensitySlider;
    delete _timeRemainSlider;
    delete _startButton;
}

