#ifndef SNOOKER_VIEW_H
#define SNOOKER_VIEW_H

#include <QGraphicsView>

class SnookerView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit SnookerView(QWidget *parent = 0);
    virtual ~SnookerView();
    void initData();
signals:

public slots:

};

#endif // SNOOKER_VIEW_H
