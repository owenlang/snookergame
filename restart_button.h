#ifndef RESTART_BUTTON_H
#define RESTART_BUTTON_H

#include <QPushButton>

class RestartButton : public QPushButton
{
    Q_OBJECT
public:
    explicit RestartButton(QWidget *parent = 0);

signals:

public slots:

};

#endif // RESTART_BUTTON_H
