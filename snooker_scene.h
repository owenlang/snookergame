#ifndef SNOOKER_SCENE_H
#define SNOOKER_SCENE_H

#include <QGraphicsScene>
#include "constant.h"

class TableItem;
class HoleItem;
class SideItem;
class BallItem;
class IntensitySlider;
class TimeRemainSlider;
class StartButton;
class QTextBrowser;
class RestartButton;
class ShowLog;

class SnookerScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit SnookerScene(QObject *parent = 0);
    virtual ~SnookerScene();
    void initTable();
    void initHole();
    void initSide();
    void initBall();
    void initSlider();
    void initData();
    bool isAllBallStop(); //台面上的球是否都是停止了
    bool haveRedBall();//台面上是否有红球
    //彩色球（Colour）：目标球红色球(1分)，共6个即黄色球(2分)、绿色球(3分)、棕色球(4分)、蓝色球(5分)、粉色球(6分)、黑色球(7分)。
    Qt::GlobalColor minScoreBall(); //反回当前台面上最小分值彩球的颜色

    static int scoreOfColor(const QColor& c)  //得到球色所对应的分值
    {
        if (c == Qt::red)       return 1;
        if (c == Qt::yellow)    return 2;
        if (c == Qt::green)     return 3;
        if (c == Qt::darkYellow)return 4;
        if (c == Qt::blue)      return 5;
        if (c == Qt::magenta)   return 6;
        if (c == Qt::black)     return 7;
        if (c == Qt::white)     return 4;
    }
    bool isFoul(const QColor& first, const QColor& should); //是否范规,//注意与彩球的等于判断,只要是其中一个就相等
public:
    BallItem* ball[BALL_NUMBER];
    BallItem* virtualBall;
    HoleItem* hole[6];
    BallItem* whiteBall;

signals:
    void showLog();
    void hideLog();

public slots:
    void moveCue(QPointF);
    void goalsBall(const QString& str, const int score);
    void resetBallPos(); //重置所有球的位置和速度

    void hideCue();
    void showCue();

    void hitBall(int &strength);

    void calcScore();
    void usageTimeSlot();
    void hitBallTimeOut();
    void showLogSlot();

    void advance();
private:
    TableItem* tableItem;

    SideItem* side0;
    SideItem* side1;
    SideItem* side2;
    SideItem* side3;

    QGraphicsLineItem *_cue;

    IntensitySlider* _intensitySlider;
    TimeRemainSlider* _timeRemainSlider;
    StartButton* _startButton;
    RestartButton* _restartButton;
    ShowLog* showLogButton;

    QTimer* _timer;

    QTextBrowser* immShow;
    QTextBrowser* recordShow;

    int actuallyScore;    
    bool isHitBall;
public:
    QColor lastBallColor;//上次打进球的颜色
    QColor ballShouldBeHitted;//应该打球的颜色
    QColor firstCollidedBallColor;//第一次与白球碰撞球的颜色

    int enteredWhiteCount;

    int score; //分数,正的表示得分,负的表示扣分
    vector <BallItem*> holeEnteredBall;//已进洞的球
//    bool foulFlag;//犯规标识变量
    int foulCount;
    int usageTime;
    int doubleHit;
    QTimer* usageTimer;
};

#endif // SNOOKER_SCENE_H
