#ifndef VECTOR2_H
#define VECTOR2_H
#include "constant.h"

class Vector2
{
public:

    Vector2() {};
    Vector2(const Vector2& v)
    {
        x = v.x;
        y = v.y;
    }
    Vector2( float xx, float yy )
    {
        x = xx;
        y = yy;
    }

    // assignment operators
    Vector2& operator += ( const Vector2& v)
    {
        x += v.x;
        y += v.y;
        return *this;
    }
    Vector2& operator -= ( const Vector2& v)
    {
        x -= v.x;
        y -= v.y;
        return *this;
    }
    Vector2& operator *= ( float f)
    {
        x *= f;
        y *= f;
        return *this;
    }
    Vector2& operator /= ( float f)
    {
        x /= f;
        y /= f;
        return *this;
    }

    // unary operators
    Vector2 operator + () const
    {
        return Vector2(x, y);
    }
    Vector2 operator - () const
    {
        return Vector2(-x, -y);
    }

    // binary operators
    Vector2 operator + ( const Vector2& v) const
    {
        return Vector2(x + v.x, y + v.y);
    }
    Vector2 operator - ( const Vector2& v) const
    {
        return Vector2(x - v.x, y - v.y);
    }
    float operator * (  const Vector2& v) const
    {
        return x * v.x + y * v.y;
    }


    Vector2 operator * ( float f) const
    {
        return Vector2(x * f, y * f);
    }
    Vector2 operator / ( float f) const
    {
        return Vector2(x / f, y / f);
    }

    float len() const
    {
        return sqrt(x * x + y * y);
    }

    const Vector2& normalize()
    {
        if( abs(len()) < EPISION ) {
            return Vector2(0, 0);
        }
        *this /= len();
        return *this;
    }
    Vector2 normal()
    {
        if( abs(len()) < EPISION )
            return Vector2(0, 0);
        return *this / len();
    }

    bool operator == ( const Vector2& v) const
    {
        return ( x == v.x && y == v.y );
    }
    bool operator != ( const Vector2& v) const
    {
        return !operator==(v);
    }



public:

    float x, y;


};
#endif // VECTOR2_H
