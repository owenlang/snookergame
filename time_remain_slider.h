#ifndef TIME_REMAIN_SLIDER_H
#define TIME_REMAIN_SLIDER_H

#include <QSlider>

class QTimer;

class TimeRemainSlider : public QSlider
{
    Q_OBJECT
public:
    explicit TimeRemainSlider(QWidget *parent = 0);
    virtual ~TimeRemainSlider();
    void restartTimer();
    void resetValue();
    void stopTimer();
signals:
    void hitBallTimeOut();

public slots:
    void timeout();

private:
    QTimer* _timer;

};

#endif // TIME_REMAIN_SLIDER_H
