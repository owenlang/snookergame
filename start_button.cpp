#include "start_button.h"
#include "StdAfx.h"

StartButton::StartButton(QWidget *parent) :
    QPushButton(parent)
{
    initStyle();
    isPause = false;
    QObject::connect(this, SIGNAL(clicked()), this, SLOT(clicked()));
}

void StartButton::initStyle()
{
    QFile file;
    file.setFileName(":/qss/qss/start_button.qss");
    file.open(QFile::ReadOnly);
    setStyleSheet(tr(file.readAll()));
    setText("Pause");
    file.close();
    this->resize(100, 30);
}

void StartButton::clicked()
{
    if (!isPause) {
        setText("Continue");
        emit pause();
    } else {
        setText("Pause");
        emit resume();
    }
    isPause = !isPause;
}

bool StartButton::getIsPause()
{
    return isPause;
}


StartButton::~StartButton()
{

}


