#include "intensity_slider.h"
#include "StdAfx.h"

IntensitySlider::IntensitySlider(QWidget *parent)
    : QSlider(parent)
{
    setOrientation(Qt::Horizontal);
    setMaximum(110);
    setMinimum(0);
    setFixedWidth(260);

    QFile file;
    file.setFileName(":/qss/qss/slider.qss");
    file.open(QFile::ReadOnly);
    setStyleSheet(QObject::tr(file.readAll()));
    file.close();

    m_value = 0;
    timer = new QTimer;
    connect(timer,SIGNAL(timeout()), this,SLOT(changeSlider()));
}

void IntensitySlider::mouseIsPressed(bool isPress)
{
    static bool first_time = true;
    if (IsSwingBall) {
        first_time = true;
        return;
    }
    if (isPress && !first_time) {
            timer->start(12);
    }
    else
    {
        timer->stop();//鼠标释放，力度条停止变化
        int strength = this->value();
        this->setValue(0);
        m_value = 0;
        //把力度值发送给白球
        emit sendStrength(strength);

        first_time = false;
    }
}

void IntensitySlider::startSliderTimer()
{
    connect(timer,SIGNAL(timeout()), this,SLOT(changeSlider()));
}

void IntensitySlider::changeSlider()
{
    static bool isContinuePress = false;
    if(m_value <= this->maximum() && !isContinuePress)
    {
        ++ m_value;
    }
    if(m_value >= this->maximum()) //使力度条从最大慢慢变小
    {
        isContinuePress = true;
    }
    if (isContinuePress) {
        -- m_value;
        if (m_value <= this->minimum()) {
            isContinuePress = false;
        }
    }
    setValue(m_value);
}

IntensitySlider::~IntensitySlider()
{

}
