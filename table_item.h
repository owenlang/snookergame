//桌子类
#ifndef TABLEITEM_H
#define TABLEITEM_H

#include <QGraphicsObject>

class TableItem : public QGraphicsObject
{
    Q_OBJECT
public:
    TableItem(QPointF mousePos = QPointF(0.0,0.0));
    //自定义table图形项目,重新实现以下两个公共虚函数
    QRectF boundingRect() const;//返回要绘制图形的矩形区域
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
public:
    QPointF getMousePos();

signals:
    void mouseIsPressed(bool);
    void mouseMove(QPointF);//信号不要实现

private:
    QPointF m_mousePos;
    //QTimer timer;
};

#endif // TABLEITEM_H
