#include "table_item.h"
#include "StdAfx.h"

TableItem::TableItem(QPointF mousePos)
{
    m_mousePos = mousePos;
    setAcceptHoverEvents(true); //是图形项支持悬停事件
}

//要绘制图形的矩形区域
QRectF TableItem::boundingRect() const
{
    qreal penWidth = 1;
    qreal x=0, y=0, width=0, height=0;
    x      = posA1.x() - penWidth/2;
    y      = posA1.y() - penWidth/2;
    width  = totalWidth - 2*posA1.x() + penWidth;
    height = 478 - 2*posA1.x()+ penWidth;
//    qDebug() << "table:" << x << "," << y << "," << width << "," << height;
    return QRectF(x, y, width, height);
}

//实际的绘图操作
void TableItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QLinearGradient lg4(posA1,posC1);//线性渐变
    lg4.setColorAt(0, QColor(0,170,0,250));
    lg4.setColorAt(0.6, QColor(50,172,40,250));
    lg4.setColorAt(1, QColor(0,170,0,250));
    lg4.setSpread(QRadialGradient::PadSpread);
    painter->setBrush(lg4);
    static const QPointF points4[4]={posA1,posB1,posC1,posD1};
    painter->drawPolygon(points4,4);

    //画开球线与D区
    QPen pen(QColor(142,212,255,150),1, Qt::SolidLine,Qt::RoundCap, Qt::RoundJoin);//定义一支白色笔
    painter->setPen(pen);//使用白色笔
    painter->drawLine(posE, posE1);//画开球线
    QRectF rectangle(128.0, 154.0, 146.0, 146.0);//定义一个矩形
    int startAngle = 90 * 16;//定义圆弧的起始角度
    int spanAngle = 180 * 16;//定义圆弧的终止角度
    painter->drawArc(rectangle, startAngle, spanAngle);//绘制D区

    //画置球点
    QBrush brush1(Qt::white,Qt::Dense4Pattern);//定义一个白色画刷
    painter->setBrush(brush1);//使用画刷
    painter->drawEllipse(posGreen,3,3);//绿球置球点
    painter->drawEllipse(posBrown,3,3);//棕球置球点
    painter->drawEllipse(posYellow,3,3);//黄置球点
    painter->drawEllipse(posBlue,3,3);//蓝置球点
    painter->drawEllipse(posPink,3,3);//粉置球点
    painter->drawEllipse(posBlack,3,3);//黑置球点

    QPen pen1(QColor(0,170,0),1,Qt::SolidLine,Qt::RoundCap,Qt::RoundJoin);
    painter->setPen(pen1);
}

void TableItem::mousePressEvent(QGraphicsSceneMouseEvent *)
{
    //传递信号给virtualBall,slider
    //slider1要能随时间变化，得到一个速度值
    emit mouseIsPressed(true);
}

void TableItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *)
{
    //把false发给slider，slider的值停止变化，得到一个值
    emit mouseIsPressed(false);

}

void TableItem::hoverEnterEvent(QGraphicsSceneHoverEvent *)
{
    //发送鼠标所处场景坐标给ball
}

void TableItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    //tableItem把鼠标位置发给虚球与白球
    m_mousePos = event->scenePos();    
    emit mouseMove(m_mousePos);
}

void TableItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *)
{
   // clearFocus();
}

QPointF TableItem::getMousePos()
{
    return m_mousePos;
}
