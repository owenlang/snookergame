//桌子的边类
#ifndef SIDEITEM_H
#define SIDEITEM_H

#include <QGraphicsObject>

class SideItem : public QGraphicsObject
{
public:
    SideItem();
    SideItem(QPointF *p);
    QRectF boundingRect() const;//返回要绘制图形的矩形区域
    //实际的绘图操作
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void setposvalue(QPointF *p);
private:
    QPointF points[4];
};

#endif // SIDEITEM_H
