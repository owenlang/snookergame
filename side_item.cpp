#include "side_item.h"
#include "StdAfx.h"

SideItem::SideItem()
{
    points[0]=QPointF(0.0,0.0);
    points[1]=QPointF(17.0,17.0);
    points[2]=QPointF(909.0,17.0);
    points[3]=QPointF(926.0,0.0);
}

SideItem::SideItem(QPointF *p)
{
    points[0]=p[0];
    points[1]=p[1];
    points[2]=p[2];
    points[3]=p[3];
}

QRectF SideItem::boundingRect() const
{
      qreal penWidth = 1;
      return QRectF(0 - penWidth/2,0 -penWidth/2,926 + penWidth,478 + penWidth);
}

void SideItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPen penGreen(QColor(0,0,0),1,Qt::SolidLine,Qt::RoundCap,Qt::RoundJoin);
    painter->setPen(penGreen);

    QLinearGradient lg(points[0],points[3]);//线性渐变
    lg.setColorAt(0,QColor(20,0,0,35));
    lg.setColorAt(0.03,QColor(122,122,0,155));
    lg.setColorAt(0.47,QColor(100,100,0,250));
    lg.setColorAt(0.5,QColor(250,0,0,35));
    lg.setColorAt(0.53,QColor(122,122,0,155));
    lg.setColorAt(0.9,QColor(100,100,0,250));
    lg.setColorAt(1,QColor(20,0,0,35));
    lg.setSpread(QRadialGradient::PadSpread);
    painter->setBrush(lg);
    painter->drawPolygon(points,4);
}

void SideItem::setposvalue(QPointF *p)
{
    points[0]=p[0];
    points[1]=p[1];
    points[2]=p[2];
    points[3]=p[3];
}

