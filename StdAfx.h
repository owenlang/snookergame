#ifndef STDAFX_H
#define STDAFX_H

#include <QtCore>
#include <QtGui>
#include <QApplication>

#include <QSlider>
#include <QGraphicsProxyWidget>
#include <QTextEdit>
#include <QPushButton>
#include <QPauseAnimation>
#include <QString>
#include <QFile>
#include <QState>
#include <QStateMachine>
#include <QSignalTransition>
#include <QPauseAnimation>
#include <QTimer>
#include <QMouseEvent>
#include <QGraphicsItem>
#include <QDebug>
#include <QColor>
#include <QPainter>
#include <QGraphicsSceneHoverEvent>
#include <QDebug>
#include <math.h>
#include <QPropertyAnimation>
#include <QPalette>
#include <QCursor>
#include <QGraphicsSceneHelpEvent>
#include <QTextBrowser>
#include <QLabel>

#include <algorithm>
#include <cstdio>

#include "constant.h"
#include "table_item.h"
#include "hole_item.h"
#include "side_item.h"
#include "ball_item.h"
#include "intensity_slider.h"
#include "time_remain_slider.h"
#include "start_button.h"
#include "snooker_scene.h"
#include "snooker_view.h"
#include "CMyWindow.h"
#include "restart_button.h"
#include "show_log.h"

#include "vector2.h"


#endif // STDAFX_H
