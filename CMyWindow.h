#ifndef CMYWINDOW_H
#define CMYWINDOW_H

#include "snooker_scene.h"
#include <QMainWindow>

class CMywindow : public QMainWindow
{
    Q_OBJECT
public:
    CMywindow(QWidget* parent=0)
	{
        resize(totalWidth+300, totalHeight + 30);//调整窗口大小到桌面大小
		myScene = new SnookerScene(this);
        myScene->setSceneRect(0, 0, totalWidth+LOG_WIDTH, totalHeight + 30);//视图的大小
		view = new SnookerView(this);
		view->setScene(myScene);
		view->centerOn(0, 0);
        view->setFixedSize(totalWidth+LOG_WIDTH, totalHeight + 32);//视口的大小
		view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        setWindowFlags(windowFlags()& ~Qt::WindowMaximizeButtonHint); //disable max window button
        QObject::connect(myScene, SIGNAL(showLog()), this, SLOT(showLog()));
        QObject::connect(myScene, SIGNAL(hideLog()), this, SLOT(hideLog()));

        hideLog();
	}
public slots:
    void showLog()
    {
        resize(totalWidth+LOG_WIDTH, totalHeight + 30);//调整窗口大小到桌面大小
        myScene->setSceneRect(0, 0, totalWidth+LOG_WIDTH, totalHeight + 30);//视图的大小
        view->setFixedSize(totalWidth+LOG_WIDTH, totalHeight + 32);//视口的大小
//        qDebug() << "show log";
    }
    void hideLog()
    {
        resize(totalWidth, totalHeight + 30);//调整窗口大小到桌面大小
        myScene->setSceneRect(0, 0, totalWidth, totalHeight + 30);//视图的大小
        view->setFixedSize(totalWidth, totalHeight + 32);//视口的大小
//        qDebug() << "hide log";
    }

//	QSize sizeHint() const
//	{
//		return QSize( 800, 600 );
//	}
private:
	SnookerScene *myScene;
	SnookerView *view;
};

#endif
