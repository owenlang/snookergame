#include "time_remain_slider.h"
#include "StdAfx.h"

TimeRemainSlider::TimeRemainSlider(QWidget *parent) :
    QSlider(parent)
{
    setOrientation(Qt::Horizontal);
    setFixedWidth(260);

    QFile file;
    file.setFileName(":/qss/qss/slider.qss");
    file.open(QFile::ReadOnly);
    setStyleSheet(QObject::tr(file.readAll()));
    file.close();

    setMaximum(30);//30秒
    setMinimum(0);
    setValue(this->maximum());

    _timer = new QTimer();
    QObject::connect(_timer, SIGNAL(timeout()), this, SLOT(timeout()));
    _timer->start(1000);
}

void TimeRemainSlider::stopTimer()
{
    _timer->stop();
    resetValue();
}

void TimeRemainSlider::resetValue()
{
    setValue(maximum());
}

void TimeRemainSlider::restartTimer()
{
    _timer->stop();
    setValue(maximum());
    _timer->start(1000);
}

void TimeRemainSlider::timeout()
{
    this->setValue(this->value()-1);
    if (this->value() == 0) {
        _timer->stop();
        emit hitBallTimeOut();
    }
}

TimeRemainSlider::~TimeRemainSlider()
{
    delete _timer;
}
