#ifndef INTENSITYSLIDER_H
#define INTENSITYSLIDER_H

#include <QSlider>

class QTimer;

class IntensitySlider : public QSlider
{
    Q_OBJECT
public:
    explicit IntensitySlider(QWidget *parent = 0);
    virtual ~IntensitySlider();
private:
    QTimer *timer;
    int m_value;

signals:
    void sendStrength(int &);

public slots:
    void mouseIsPressed(bool x);
    void changeSlider();
    void startSliderTimer();
};

#endif // MYSLIDERITEM_H
