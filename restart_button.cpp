#include "restart_button.h"
#include "StdAfx.h"

RestartButton::RestartButton(QWidget *parent) :
    QPushButton(parent)
{
    QFile file;
    file.setFileName(":/qss/qss/start_button.qss");
    file.open(QFile::ReadOnly);
    setStyleSheet(tr(file.readAll()));
    setText("Restart");
    file.close();
    this->resize(100, 30);
}
