//球洞类
#ifndef HOLEITEM_H
#define HOLEITEM_H
#include <QGraphicsObject>

class HoleItem : public QGraphicsObject
{
private:
    QPoint pos;    //椭圆中心
    int angle;     //坐标旋转角度
    QColor color;  //颜色
    const static int x = 13;
    const static int y = 11;
public:
	QPoint getPos()
	{
		return pos;
	}
    HoleItem(QPoint po=QPoint(17,17),
             int ang=0,
             QColor cl=Qt::black);
    QRectF boundingRect() const;//返回要绘制图形的矩形区域
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // HOLEITEM_H
